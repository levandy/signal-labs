import csv
import numpy as np
import math
import matplotlib.pyplot as plt


def run():
    amp_sig = 1.0
    snr = 2.0
    x = np.array([])
    signal = np.array([])
    noise = np.array([])

    # read csv with signal
    with open('../1/10_signal.csv', 'r') as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        for row in reader:
            # x = np.append(x, float(row[0]))
            signal = np.append(signal, float(row[0]))

    # read csv with noise
    with open('../2/10_noise.csv', 'r') as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        for row in reader:
            noise = np.append(noise, float(row[0]))

    # SNR = 20*log10(Amp_signal / Amp_noise)
    summ = 0
    for i in signal:
        summ += pow(i, 2)
    amp_sig = math.sqrt(summ/len(signal))


    summ = 0
    for i in noise:
        summ += pow(i, 2)
    amp_noise_csv = math.sqrt(summ / len(noise))

    amp_noise = amp_sig / (10 ** (snr / 20))
    k = amp_noise / amp_noise_csv

    # get signal+noise*k data
    sig_and_noise = signal + noise * k
    # plot
    # plt.figure(figsize=(15, 10), dpi=250)
    # plt.plot(x[::10], sig_and_noise[::10], color='g', linewidth=0.5)
    # plt.title('Signal and noise (10%)')
    # plt.ylabel('Amplitude, V')
    # plt.xlabel('Time, Sec')
    # plt.savefig('1.png')

    np.savetxt('10_sn.csv', sig_and_noise, delimiter=',')
    plt.clf()


if __name__ == "__main__":
    run()
