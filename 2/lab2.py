import numpy as np
import matplotlib.pyplot as plt
from collections import Counter


def run():
    # число отсчетов
    counts_num = 10000
    # мат ожидание
    mu = 0
    # среднеквадратичное отклонение
    sigma = 0.25

    # generate data
    y = np.random.normal(mu, sigma, size=counts_num)
    x = np.linspace(0, 0.1, counts_num)
    # save data to csv
    np.savetxt('10_noise.csv', y, delimiter=',')

    # plot
    plt.figure(figsize=(15, 10), dpi=300)
    plt.plot(x[::10], y[::10], color='g', linewidth=0.5)
    plt.title('Noise (10%)')
    plt.ylabel('Amplitude, V')
    plt.xlabel('Time, sec')
    # save plot
    plt.savefig('1.png')
    plt.clf()
    # мат ожидание
    counter = Counter(y)
    expected = 0
    for i in y:
        expected += i*counter.get(i)/counts_num
    # выборочная дисперсия
    var = 0
    for i in y:
        var += pow(i - expected, 2) * counter.get(i) / counts_num
    print("Expected value = \t\t"+str(expected))
    print("Sample variance = \t"+str(var))


if __name__ == "__main__":
    run()
