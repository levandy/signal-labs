import numpy as np
import matplotlib.pyplot as plt


def run():
    # амплитуда
    amplitude = 1.0
    # смещение
    shift = 0.0
    # частота
    frequency = 23193.35983
    # нач. фаза (град.)
    phase_grad = 90
    # частота дискретизации
    sampling_frequency = 100000
    # время моделирования
    modeling_time = 0.1

    # get characteristics
    cycle_frequency = 2 * np.pi * frequency
    phase_rad = phase_grad * np.pi / 180.0
    sampling_count = int(sampling_frequency * modeling_time)

    print("Частота в модели = "+str(cycle_frequency))
    print("Начальная фаза (рад.) = "+str(phase_rad))
    print("Число отсчетов = "+str(sampling_count))

    # get data
    x = np.linspace(0.0, modeling_time, sampling_count)
    y = amplitude * np.cos(cycle_frequency * x + phase_rad) + shift
    # save data to csv
    np.savetxt('10_signal.csv', y, delimiter=',')
    # график
    plt.figure(figsize=(15, 10), dpi=250)
    plt.plot(x, y, color='g', linewidth=0.5)
    plt.title('Signal')
    plt.ylabel('Amplitude, V')
    plt.xlabel('Time, Sec')
    # plt.show()
    plt.savefig('1.png')

    # график укрупненно
    plt.figure(figsize=(15, 10), dpi=250)
    plt.plot(x[::10], y[::10], color='b', linewidth=0.5)
    plt.title('Partitioned signal (10%)')
    plt.ylabel('Amplitude, V')
    plt.xlabel('Time, Sec')
    # plt.show()
    plt.savefig('2.png')


if __name__ == "__main__":
    run()
